var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var expressValidator = require('express-validator');
var expressSession = require('express-session');

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('public'));
app.use(cookieParser());
app.use(expressValidator());
app.set('view engine', 'ejs');

var contacts = [];

app.get('/', function(req, res){
    res.render('home', {contacts: contacts});
});

app.post('/addcontact', function(req, res){
    var newContact = req.body.newcontact;
    contacts.push(newContact);
    res.redirect('/contacts');
});

app.get('/contacts', function(req, res){
    res.render('home', {contacts: contacts});
});

app.get('/list', function(req, res){
    res.render('list', {list: JSON.stringify(contacts)});
});

app.get('/headers', function(req, res){
    res.render('info', {headers: JSON.stringify(req.headers)});
});

app.get('/cookies', function(req, res){
    res.cookie('contact1', 'Bob', {maxAge: 5000});
    res.cookie('contact2', 'Bill', {maxAge: 5000});
    res.cookie('contact3', 'Jane', {maxAge: 5000});
    res.cookie('contact4', 'John', {maxAge: 5000});
    res.cookie('contact5', 'Jill', {maxAge: 5000});
    res.render('cookies', {cookies: JSON.stringify(req.cookies)});
});

app.get('*', function(req, res){
    res.send('Get out of here!');
})

app.listen(8080, '127.0.0.1', function(req, res){
    console.log('Connecting to port 8080...');
});